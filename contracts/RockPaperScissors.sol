// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;


contract RockPaperScissors {
    event MyEvent (
      bytes32 data
    );

    // There are two players in this game and each of them makes a choice
    address payable public player1;
    address payable public player2;
    string public choiceOfPlayer1;
    string public choiceOfPlayer2;
    bytes32 public commitmentOfPlayer1;
    bytes32 public commitmentOfPlayer2;
    bool public hasPlayer1MadeCommit;
    bool public hasPlayer2MadeCommit;
    bool public hasPlayer1MadeChoice;
    bool public hasPlayer2MadeChoice;
    
    // When a player joins the game, they have to pay a playing fee 
    uint256 public stake; //the stake should be visible to Player2

    // A matrix containing result of the game depedning on its states
    mapping(string => mapping(string => uint8)) public states;

    // The constructor initialise the game environment
    constructor() {
        states['R']['R'] = 0;
        states['R']['P'] = 2;
        states['R']['S'] = 1;
        states['P']['R'] = 1;
        states['P']['P'] = 0;
        states['P']['S'] = 2;
        states['S']['R'] = 2;
        states['S']['P'] = 1;
        states['S']['S'] = 0;

        stake = 1 ether;
    }
    
    // Modifiers
    
    modifier isJoinable() {
        require(player1 == address(0) || player2 == address(0),
                "The room is full."
        );
        require((player1 != address(0) && msg.value == stake) || (player1 == address(0)), //Player1 can choose the stake, Player2 has to match. 
                "You must pay the stake to play the game."
        );
        _;
    }
    
    modifier isPlayer() {
        require(msg.sender == player1 || msg.sender == player2,
                "You are not playing this game."
        );
        _;
    }

    modifier playersMadeCommit() {
        require(hasPlayer1MadeCommit && hasPlayer2MadeCommit,
                "The player(s) have not made their commit yet."
        );
        _;
    }
    
    modifier playersMadeChoice() {
        require(hasPlayer1MadeChoice && hasPlayer2MadeChoice,
                "The player(s) have not made their choice yet."
        );
        _;
    }

    // Functions
     
    function join() external payable  isJoinable() { // To join the game, there must be a free space
        // emit MyEvent(keccak256(abi.encodePacked("R", "123")));
        // emit MyEvent(keccak256(abi.encodePacked("P", "456")));
        if (player1 == address(0)) {
            player1 = msg.sender;
            stake = msg.value; //Player1 determines the stake
        } else if (player2 == address(0)){
            player2 = msg.sender;
        } else {
            require(false,
                "The player(s) have not made their choice yet."
            );
            require(true,
                "The player(s) have not made their choice yet. t"
            );
        }
    }
    
    function commit(bytes32 commitment) external isPlayer() {
        if (msg.sender == player1 && !hasPlayer1MadeChoice) {
            commitmentOfPlayer1 = commitment;
            hasPlayer1MadeCommit = true;
        } else if (msg.sender == player2 && !hasPlayer2MadeChoice) {
            commitmentOfPlayer2 = commitment;
            hasPlayer2MadeCommit = true;
        }
    }

    function reveal(string calldata choice, string calldata blinding_factor) external
        isPlayer()
        playersMadeCommit()
    {
        if (msg.sender == player1) {
            require(keccak256(abi.encodePacked(choice, blinding_factor)) == commitmentOfPlayer1, "invalid hash");
            hasPlayer1MadeChoice = true;
            choiceOfPlayer1 = choice;
        } else if (msg.sender == player2) {
            require(keccak256(abi.encodePacked(choice, blinding_factor)) == commitmentOfPlayer2, "invalid hash");
            hasPlayer2MadeChoice = true;
            choiceOfPlayer2 = choice;
        }
    }
    
    function disclose() external 
        isPlayer()          // Only players can disclose the game result
        playersMadeChoice() // Can disclose the result when choices are made
    {
        // Disclose the game result
        int result = states[choiceOfPlayer1][choiceOfPlayer2];
        if (result == 0) {
            player1.transfer(stake); 
            player2.transfer(stake);
        } else if (result == 1) {
            player1.transfer(address(this).balance);
        } else if (result == 2) {
            player2.transfer(address(this).balance);
        }
        
        // Reset the game
        player1 = address(0);
        player2 = address(0);

        choiceOfPlayer1 = "";
        choiceOfPlayer2 = "";
        
        hasPlayer1MadeChoice = false;
        hasPlayer2MadeChoice = false;
        
        stake = 1 ether;
    }
}
