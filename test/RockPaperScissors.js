const RockPaperScissors = artifacts.require("RockPaperScissors");

contract('RockPaperScissors', (accounts) => {
    
    it('initial test', async () => {
        const game = await RockPaperScissors.deployed();
        player1 = await game.player1.call();
        player2 = await game.player2.call();
        choiceOfPlayer1 = await game.choiceOfPlayer1.call();
        choiceOfPlayer2 = await game.choiceOfPlayer2.call();
        commitmentOfPlayer1 = await game.commitmentOfPlayer1.call();
        commitmentOfPlayer2 = await game.commitmentOfPlayer2.call();
        hasPlayer1MadeCommit = await game.hasPlayer1MadeCommit.call();
        hasPlayer2MadeCommit = await game.hasPlayer2MadeCommit.call();
        hasPlayer1MadeChoice = await game.hasPlayer1MadeChoice.call();
        hasPlayer2MadeChoice = await game.hasPlayer2MadeChoice.call();
        stake = await game.stake.call();


        assert.equal(player1, '0x0000000000000000000000000000000000000000');
        assert.equal(player2, '0x0000000000000000000000000000000000000000');

        assert.equal(choiceOfPlayer1, '');
        assert.equal(choiceOfPlayer2, '');
        assert.equal(commitmentOfPlayer1, '0x0000000000000000000000000000000000000000000000000000000000000000');
        assert.equal(commitmentOfPlayer2, '0x0000000000000000000000000000000000000000000000000000000000000000');
        
        assert.equal(hasPlayer1MadeCommit, false);
        assert.equal(hasPlayer2MadeCommit, false);
        assert.equal(hasPlayer1MadeChoice, false);
        assert.equal(hasPlayer2MadeChoice, false);
        assert.equal(stake.toString(10), '1000000000000000000'); // 1 eth
    });

    it('join game', async () => {
        const game = await RockPaperScissors.deployed();
        balance_before = await web3.eth.getBalance(accounts[0]);
        await game.join({from: accounts[0], value: web3.utils.toWei('2', 'ether')})
        
        player1 = await game.player1.call();
        stake = await game.stake.call();
        balance_after = await web3.eth.getBalance(accounts[0]);

        assert.equal(player1, accounts[0]);
        assert.equal(stake.toString(10), '2000000000000000000'); // 2 eth
        assert.ok(
            balance_before - balance_after - web3.utils.toWei('2', 'ether')
            < web3.utils.toWei('5', 'finney') // estimated gas price limit
        )

        try {
            await game.join({from: accounts[1], value: web3.utils.toWei('1', 'ether')})
            assert.fail('expected error about size of stake')
        } catch(err) {}

        player2 = await game.player2.call();
        assert.equal(player2, '0x0000000000000000000000000000000000000000');


        await game.join({from: accounts[2], value: web3.utils.toWei('2', 'ether')})
        player2 = await game.player2.call();
        assert.equal(player2, accounts[2]);
    });

    it('commit game', async() => {
        const game = await RockPaperScissors.deployed();

        await game.commit(
            '0xff69e414b82ba18a659cba62b3f7a381307b13d208c48bf2d102a9c5f85ab766',
            {from: accounts[2]}
        )// P 456
        await game.commit(
            '0xfff3dda3031feb6a638618967bc03b79c5593f266a4e6b14c17a049c9e889046',
            {from: accounts[0]}
        )// R 123

        commitmentOfPlayer1 = await game.commitmentOfPlayer1.call();
        commitmentOfPlayer2 = await game.commitmentOfPlayer2.call();
        assert.equal(commitmentOfPlayer1, '0xfff3dda3031feb6a638618967bc03b79c5593f266a4e6b14c17a049c9e889046');
        assert.equal(commitmentOfPlayer2, '0xff69e414b82ba18a659cba62b3f7a381307b13d208c48bf2d102a9c5f85ab766');
    });


    it('reveal game', async() => {
        const game = await RockPaperScissors.deployed();

        try {
            await game.reveal('S', '123', {from: accounts[0]}) // wrong
            assert.fail('expected wrong hash')
        } catch(err) {}
        
        await game.reveal('P', '456', {from: accounts[2]})
        await game.reveal('R', '123', {from: accounts[0]})

        choiceOfPlayer1 = await game.choiceOfPlayer1.call();
        choiceOfPlayer2 = await game.choiceOfPlayer2.call();

        assert.equal(choiceOfPlayer1, 'R');
        assert.equal(choiceOfPlayer2, 'P');
    });

    it('finish game', async() => {
        const game = await RockPaperScissors.deployed();
        balance_before = await web3.eth.getBalance(accounts[2]);
        
        await game.disclose({from: accounts[0]})

        balance_after = await web3.eth.getBalance(accounts[2]);
        assert.equal(
            balance_after - balance_before,
            web3.utils.toWei('4', 'ether')
        ) // gas payed by player2
    });
});
